#include "../includes/board_state.h"
#include "../includes/file_i.h"
#include <iostream>

const std::string file_path = "../data/tic-tac-toe.data.txt";

int main(int argc, char** argv) {
    std::vector<BoardState> states;
    read_data_file(file_path, states);
    
    // Calculate average distance between objects
    double avg_dist = 0.0;
    double op_avg_dist = 0.0;
    double n_samples_pos = 0.0;
    double n_samples_neg = 0.0;

    for (unsigned int i = 0; i < states.size(); ++i) {
        for (unsigned int j = i + 1; j < states.size(); ++j) {
            if (states[i].x_wins && states[j].x_wins) {
                avg_dist += states[i].distance(states[j]);
                n_samples_pos++;
            }
            else if (states[i].x_wins && !states[j].x_wins) {
                op_avg_dist += states[i].distance(states[j]);
                n_samples_neg++;
            }
        }
    }

    std::cout << "Average distance: " << avg_dist / n_samples_pos << std::endl;
    std::cout << "Average distance to opposite Cluster: " << op_avg_dist / n_samples_neg << std::endl;

    return 0;
}