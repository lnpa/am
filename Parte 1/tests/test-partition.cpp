#include "../includes/board_state.h"
#include "../includes/file_i.h"
#include "../includes/fuzzy_clustering.h"
#include <chrono>
#include <random>
#include <iostream>
#include <string>

const std::string file_path = "../data/tic-tac-toe.data.txt";
const int iterations = 1000;
const bool hand_crafted = false;

int main() {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);

    std::vector<BoardState> states;
    read_data_file(file_path, states);
    FuzzyClustering fc(2, 2.0, 150, 10e-10, 2, states);

    // Testing input
    //for (auto& state : states) std::cout << state << std::endl;

    // Testing board states id
    /*for (auto& state : states) {
        std::cout << state << std::endl;
    }*/

    // Testing distance function
    /*std::uniform_int_distribution<int> distribution(0, static_cast<int>(states.size()));

    for (int i = 0; i < 10; ++i) {
        int b1 = distribution(generator);
        int b2 = distribution(generator);
        std::cout << "Board #" << b1 << ": " << states[b1] << std::endl;
        std::cout << "Board #" << b2 << ": " << states[b2] << std::endl;
        std::cout << "Distance: " << states[b1].distance(states[b2]) << std::endl;
        std::cout << std::endl;
    }*/

    // Testing random prototype generation
    /*for (int i = 0; i < iterations; ++i) {
        fc.random_prototypes();
        std::set<BoardState> tmp;
        std::cout << "G_" << i << ":";
        for (auto& x : fc.get_G()) {
            for (auto& y : x)
                std::cout << y << " ";
            if (i > 0) {
                if (tmp == x) {
                    std::cout << "ERROR" << std::endl;
                    break;
                }
            }
            tmp = x;
        }
        std::cout << std::endl;
    }
    std::cout << "All " << iterations << " tests executed successfully." << std::endl;*/
    
    // Testing best partition computation
    if (hand_crafted) {
        auto E = fc.get_E();
        BoardState bs1("oxobxobxx", true, 339);
        BoardState bs2("xxxxboobo", true, 10);
        BoardState bs3("xooxboxbx", true, 166);
        BoardState bs4("xxxoxoobb", true, 15);
        std::set<BoardState> s1;
        std::set<BoardState> s2;
        s1.insert(bs1);
        s1.insert(bs2);
        s2.insert(bs3);
        s2.insert(bs4);
        std::vector<std::set<BoardState>> g;
        g.push_back(s1);
        g.push_back(s2);

        fc.set_G(g);   
    } else {
        //fc.random_prototypes();
        fc.initialize();
        fc.compute_prototypes();
    }

    fc.compute_partition();
    
    int i = 1;
    for (auto& x : fc.get_G()) {
        std::cout << "Prototype #" << i << ": ";
        for (auto& y : x) std::cout << y << " ";
        std::cout << std::endl;
        ++i;
    }

    i = 0;
    int k = 0;
    for (auto& x : fc.get_U()) {
        for (auto& y : x) {
            std::cout << "u_" << i << "_" << k << ": " << y << " ";
            ++k;
        }
        std::cout << std::endl;
        ++i;
        k = 0;
    }

    return 0;
}