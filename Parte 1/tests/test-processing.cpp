#include "../includes/board_state.h"
#include "../includes/file_i.h"
#include "../includes/fuzzy_clustering.h"
#include "../includes/post_processing.h"
#include <iostream>

const std::string file_path = "../data/tic-tac-toe.data.txt";

int main(int argc, char** argv) {
    std::vector<BoardState> states;
    read_data_file(file_path, states);
    FuzzyClustering fc(2, 2.0, 150, 10e-10, 2, states);

    std::vector<std::vector<double>> fuzzy;
    get_best_result(fc, 10, fuzzy);

    std::vector<std::vector<bool>> hard;
    get_hard_partition(fuzzy, hard);

    std::vector<BoardState> E = fc.get_E();

    //print_fuzzy_partition(fuzzy, E);
    //print_hard_partition(hard, E);

    double ari = adjusted_rand_index(hard, E);

    std::cout << "ari: " << ari << std::endl;

    /*std::cout << n_choose_2(958) << std::endl;
    std::cout << n_choose_2(10) << std::endl;
    std::cout << n_choose_2(31) << std::endl;
    std::cout << n_choose_2(2) << std::endl;*/

    return 0;
}