#include <chrono>
#include <random>
#include <iostream>
#include <string>
#include "../includes/board_state.h"
#include "../includes/file_i.h"
#include "../includes/fuzzy_clustering.h"

int main() {
    // Testing adequancy - small case
    std::vector<BoardState> states;
    BoardState bs1("oxobxobxx", true, 0);
    BoardState bs2("xxxxboobo", true, 1);
    BoardState bs3("xooxboxbx", true, 2);
    BoardState bs4("xxxoxoobb", true, 3);

    states.push_back(bs1);
    states.push_back(bs2);
    states.push_back(bs3);
    states.push_back(bs4);

    FuzzyClustering fc(2, 2.0, 150, 10e-10, 2, states);
    std::set<BoardState> s1;
    std::set<BoardState> s2;
    s1.insert(bs1);
    s1.insert(bs2);
    s2.insert(bs3);
    s2.insert(bs4);
    std::set<std::set<BoardState>> g;
    g.insert(s1);
    g.insert(s2);
    fc.set_G(g); 
    fc.compute_partition();

    int i = 1;
    for (auto& x : fc.get_G()) {
        std::cout << "Prototype #" << i << ": ";
        for (auto& y : x) std::cout << y << ",\n";
        std::cout << std::endl;
        ++i;
    }

    std::cout << "Objects: " << std::endl;
    for (auto& e : fc.get_E()) {
        std::cout << e << std::endl;
    }

    std::cout << "\n";
    std::cout << "Dist" << std::endl;
    for (auto& row : fc.get_D()) {
        for (auto& elm : row) {
            std::cout << elm << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";

    i = 0;
    int k = 0;
    for (auto& x : fc.get_U()) {
        for (auto& y : x) {
            std::cout << "u_" << i << "_" << k << ": " << y << " ";
            ++k;
        }
        std::cout << std::endl;
        ++i;
        k = 0;
    }

    double J = fc.compute_adequancy();
    std::cout << "J = " << J << std::endl;

    return 0;
}