#include "../includes/board_state.h"
#include "../includes/file_i.h"
#include "../includes/fuzzy_clustering.h"
#include <iostream>

const std::string file_path = "../data/tic-tac-toe.data.txt";

int main(int argc, char** argv) {
    std::vector<BoardState> states;
    read_data_file(file_path, states);
    FuzzyClustering fc(2, 2.0, 150, 10e-10, 2, states);

    fc.run();

    /*int i = 0;
    int k = 0;
    for (auto& x : fc.get_U()) {
        for (auto& y : x) {
            std::cout << "u_" << i << "_" << k << ": " << y << " ";
            ++k;
        }
        std::cout << std::endl;
        ++i;
        k = 0;
    }*/

    std::cout.precision(20);
    std::cout << "J: " << fc.get_J() << std::endl; 

    return 0;
}