#include <chrono>
#include <random>
#include <iostream>
#include <string>
#include "../includes/board_state.h"
#include "../includes/file_i.h"
#include "../includes/fuzzy_clustering.h"

const std::string file_path = "../data/tic-tac-toe.data.txt";
const int iterations = 1000;
const bool hand_crafted = false;

int main() {
    //unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    //std::default_random_engine generator(seed);

    std::vector<BoardState> states;

    states.push_back(BoardState("xxxxooxoo", true, 0));
    states.push_back(BoardState("xxxxoooxo", true, 1));
    states.push_back(BoardState("xxxxoooox", true, 2));
    states.push_back(BoardState("xxxxooobb", true, 3));
    states.push_back(BoardState("xxxxoobob", true, 4));
    states.push_back(BoardState("bxoxbobxo", false, 5));
    states.push_back(BoardState("bxoooxoxx", false, 6));
    states.push_back(BoardState("bxobxoxbo", false, 7));
    states.push_back(BoardState("bxoboxoxb", false, 8));
    states.push_back(BoardState("bxoboxobx", false, 9));

    FuzzyClustering fc(2, 2, 150, 10e-10, 2, states);

    std::set<std::set<BoardState>> g;
    std::set<BoardState> g1, g2;
    g1.insert(states[0]);
    g1.insert(states[1]);
    g2.insert(states[5]);
    g2.insert(states[6]);
    g.insert(g1);
    g.insert(g2);

    fc.set_G(g);
    fc.compute_partition();
    fc.compute_prototypes();

    int i = 1;
    for (auto& x : fc.get_G()) {
        std::cout << "Prototype #" << i << ": ";
        for (auto& y : x) std::cout << y << ", ";
        std::cout << std::endl << std::endl;
        ++i;
    }

    std::cout << "Objects: " << std::endl;
    for (auto& e : fc.get_E()) {
        std::cout << e << std::endl;
    }
    std::cout << std::endl;

    i = 0;
    int k = 0;
    for (auto& x : fc.get_U()) {
        for (auto& y : x) {
            std::cout << "u_" << i << "_" << k << ": " << y << " ";
            ++k;
        }
        std::cout << std::endl;
        ++i;
        k = 0;
    }
    std::cout << std::endl;

    for (int i = 0; i < (int) states.size(); i++){
        std::cout << "E_" << i << ": {";
        for (int j = 0; j < (int) states.size(); j++){
            std::cout << "E_" << j << ": " << states[i].distance(states[j]);
            if (j == ((int) states.size())-1){
                std::cout << "}" << std::endl;
            } else {
                std::cout << ", ";
            }
        }
    }

    //read_data_file(file_path, states);
    // Testing board states id
    /*for (auto& state : states) {
        std::cout << state << std::endl;
    }*/
    //FuzzyClustering fc(2, 2, 150, 10e-10, 2, states);

    // Testing input
    //for (auto& state : states) std::cout << state << std::endl;

    // Testing distance function
    /*std::uniform_int_distribution<int> distribution(0, static_cast<int>(states.size()));

    for (int i = 0; i < 10; ++i) {
        int b1 = distribution(generator);
        int b2 = distribution(generator);
        std::cout << "Board #" << b1 << ": " << states[b1] << std::endl;
        std::cout << "Board #" << b2 << ": " << states[b2] << std::endl;
        std::cout << "Distance: " << states[b1].distance(states[b2]) << std::endl;
        std::cout << std::endl;
    }*/

    // Testing random prototype generation
    /*for (int i = 0; i < iterations; ++i) {
        fc.random_prototypes();
        std::set<BoardState> tmp;
        for (auto& x : fc.get_G()) {
            if (i > 0) {
                if (tmp == x) {
                    std::cout << "ERROR" << std::endl;
                    break;
                }
            }
            tmp = x;
        }
    }
    std::cout << "All " << iterations << " tests executed successfully." << std::endl;*/

    // Testing best partition computation
    
    // Hand-crafted case
    /*if (hand_crafted) {
        BoardState bs1("oxobxobxx", true, 0);
        BoardState bs2("xxxxboobo", true, 1);
        BoardState bs3("xooxboxbx", true, 2);
        BoardState bs4("xxxoxoobb", true, 3);
        std::set<BoardState> s1;
        std::set<BoardState> s2;
        s1.insert(bs1);
        s1.insert(bs2);
        s2.insert(bs3);
        s2.insert(bs4);
        std::set<std::set<BoardState>> g;
        g.insert(s1);
        g.insert(s2);

        fc.set_G(g);   
    } else {
        fc.random_prototypes();
    }

    fc.compute_partition();
    
    int i = 1;
    for (auto& x : fc.get_G()) {
        std::cout << "Prototype #" << i << ": ";
        for (auto& y : x) std::cout << y << " ";
        std::cout << std::endl;
        ++i;
    }

    i = 0;
    int k = 0;
    for (auto& x : fc.get_U()) {
        for (auto& y : x) {
            std::cout << "u_" << i << "_" << k << ": " << y << " ";
            ++k;
        }
        std::cout << std::endl;
        ++i;
        k = 0;
    }*/
    
    /*
    // Testing adequancy - small case
    BoardState bs1("oxobxobxx", true, 0);
    BoardState bs2("xxxxboobo", true, 1);
    BoardState bs3("xooxboxbx", true, 2);
    BoardState bs4("xxxoxoobb", true, 3);

    states.push_back(bs1);
    states.push_back(bs2);
    states.push_back(bs3);
    states.push_back(bs4);

    FuzzyClustering fc(2, 2, 150, 10e-10, 2, states);
    std::set<BoardState> s1;
    std::set<BoardState> s2;
    s1.insert(bs1);
    s1.insert(bs2);
    s2.insert(bs3);
    s2.insert(bs4);
    std::set<std::set<BoardState>> g;
    g.insert(s1);
    g.insert(s2);
    fc.set_G(g); 
    fc.compute_partition();

    int i = 1;
    for (auto& x : fc.get_G()) {
        std::cout << "Prototype #" << i << ": ";
        for (auto& y : x) std::cout << y << ",\n";
        std::cout << std::endl;
        ++i;
    }

    std::cout << "Objects: " << std::endl;
    for (auto& e : fc.get_E()) {
        std::cout << e << std::endl;
    }

    i = 0;
    int k = 0;
    for (auto& x : fc.get_U()) {
        for (auto& y : x) {
            std::cout << "u_" << i << "_" << k << ": " << y << " ";
            ++k;
        }
        std::cout << std::endl;
        ++i;
        k = 0;
    }

    double J = fc.compute_adequancy();
    std::cout << "J = " << J << std::endl;
    */
    return 0;
}