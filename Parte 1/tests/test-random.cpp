#include "../includes/board_state.h"
#include "../includes/file_i.h"
#include "../includes/fuzzy_clustering.h"
#include <chrono>
#include <random>
#include <iostream>
#include <string>

const std::string file_path = "../data/tic-tac-toe.data.txt";
const int iterations = 1000;
const bool hand_crafted = false;

int main() {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);

    std::vector<BoardState> states;
    read_data_file(file_path, states);
    FuzzyClustering fc(2, 2.0, 150, 10e-10, 2, states);

    fc.random_prototypes();

    return 0;
}

