#include "../includes/board_state.h"
#include "../includes/fuzzy_clustering.h"
#include <chrono>
#include <cmath>
#include <iostream>
#include <functional>
#include <limits>
#include <random>
#include <set>

FuzzyClustering::FuzzyClustering(int K, double m, int T, double eps, int q, std::vector<BoardState>& E) {
    this->K = K;
    this->m = m;
    this->T = T;
    this->eps = eps;
    this->q = q;
    this->E = E;
    this->n = this->E.size();
    calculate_dissimilarity();

    // Initialize RNG
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    this->rng = Rand_Int(seed, 0, this->n - 1);
}

// Calculate Dissimilarity matrix from objects
void FuzzyClustering::calculate_dissimilarity() {
    for (int i = 0; i < this->n; ++i) {
        std::vector<int> row;
        for (int j = 0; j < this->n; ++j) {
            int dist = this->E[i].distance(this->E[j]);
            row.push_back(dist);
        }
        this->D.push_back(row);
    }
}

// Randomly select K distinct prototype Gk(0) from {A c E : |A|  = q}
// Warning: Non-deterministic with a extremely low chance
void FuzzyClustering::random_prototypes() {
    this->G.clear();
    while (static_cast<int>(this->G.size()) < this->K) {
        std::set<BoardState> distinct_prototype;

        // Build prototype of q distinct elements from E
        while (static_cast<int>(distinct_prototype.size()) < this->q) {
            int random_position = this->rng();
            BoardState bs = this->E[random_position];
            distinct_prototype.insert(bs);
        }

        bool already_inserted = false;
        for (auto& x : this->G) {
            if (x == distinct_prototype)
                already_inserted = true;
        }
        if (already_inserted) continue;
        this->G.push_back(distinct_prototype);
    }
}

// Initialization step
void FuzzyClustering::initialize() { 
    this->random_prototypes();
    this->compute_partition();
    this->J = this->compute_adequancy();
}

// Step 1 procedure for best prototypes computation
void FuzzyClustering::compute_prototypes() {
    std::vector<std::set<BoardState>> G(2);

    int k = 0;

    while (k < this->K) {
        int min_h = 0;
        double min_sum = std::numeric_limits<double>::max();
        
        for (int h = 0; h < static_cast<int>(this->E.size()); h++) {
            double sum = 0;
            for (int i = 0; i < this->n; i++)
                sum += this->D[h][i] * pow(this->U[i][k], this->m);
            //summations[k].push_back(sum);
            bool already_chosen = G[k].count(this->E[h]);
            if (sum < min_sum && !already_chosen) {
                min_h = h;
                min_sum = sum;
            }
        }
        G[k].insert(this->E[min_h]);
        if (static_cast<int>(G[k].size()) == this->K)
            k++;
    }

    this->G = G;
}

// Step 2 function for best fuzzy partition computation
void FuzzyClustering::compute_partition() {
    this->U.clear();

    for (auto& e_i : this->E) {
        std::vector<double> u_i;

        for (auto& g_k : this->G) {
            double u_ik = 0.0;

            for (auto& g_h : this->G) {
                int numerator = 0;
                int denominator = 0;

                for (auto& e : g_k) {
                    numerator += this->D[e_i.id][e.id];
                }

                for (auto& e : g_h) {
                    denominator += this->D[e_i.id][e.id];
                }

                double frac = static_cast<double>(numerator) / 
                              static_cast<double>(denominator);

                // (n/d) ^ (1 / (m - 1))
                u_ik += pow(frac, (1.0 / (this->m - 1.0)));
            }

            u_ik = pow(u_ik, -1.0);
            
            u_i.push_back(u_ik);
        }

        this->U.push_back(u_i);
    }
}

// Adequancy criterion computation
double FuzzyClustering::compute_adequancy() {
    double j = 0.0;
    for (int k = 0; k < this->K; ++k) {
        for (int i = 0; i < this->n; ++i) {
            double factor_1 = pow(this->U[i][k], this->m);
            double factor_2 = 0.0;
            for (auto& e : this->G[k]) {
                factor_2 += this->D[i][e.id];
            }
            j += (factor_1 * factor_2);
        }
    }
    return j;
}

// Run full algorithm for T iterations
void FuzzyClustering::run() {
    this->initialize();
    for (int t = 0; t < this->T; ++t) {
        this->compute_prototypes();
        this->compute_partition();
        double J_t = this->compute_adequancy();
        double J_t1 = this->J;
        this->J = J_t;
        if (this->compare(J_t, J_t1)) {
            break;          
        }
    }

}

/* ------- */
/* Getters */
/* ------- */

int FuzzyClustering::get_K() {
    return this->K;
}

double FuzzyClustering::get_m() {
    return this->m;
}

int FuzzyClustering::get_T() {
    return this->T;
}

int FuzzyClustering::get_q() {
    return this->q;
}

int FuzzyClustering::get_n() {
    return this->n;
}

double FuzzyClustering::get_eps() {
    return this->eps;
}

double FuzzyClustering::get_J() {
    return this->J;
}

std::vector<BoardState> FuzzyClustering::get_E() {
    return this->E;
}

std::vector<std::set<BoardState>> FuzzyClustering::get_G() {
    return this->G;
}

std::vector<std::vector<double>> FuzzyClustering::get_U() {
    return this->U;
}

std::vector<std::vector<int>> FuzzyClustering::get_D() {
    return this->D;
}

/* ------- */
/* Setters */
/* ------- */

void FuzzyClustering::set_K(int K) {
    this->K = K;
}

void FuzzyClustering::set_m(double m) {
    this->m = m;
}

void FuzzyClustering::set_T(int T) {
    this->T = T;
}

void FuzzyClustering::set_q(int q) {
    this->q = q;
}

void FuzzyClustering::set_n(int n) {
    this->n = n;
}

void FuzzyClustering::set_eps(double eps) {
    this->eps = eps;
}

void FuzzyClustering::set_J(double J) {
    this->J = J;
}

void FuzzyClustering::set_E(std::vector<BoardState>& E) {
    this->E = E;
}

void FuzzyClustering::set_G(std::vector<std::set<BoardState>>& G) {
    this->G = G;
}

void FuzzyClustering::set_U(std::vector<std::vector<double>>& U) {
    this->U = U;
}

void FuzzyClustering::set_D(std::vector<std::vector<int>>& D) {
    this->D = D;
}