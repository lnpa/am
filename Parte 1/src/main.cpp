#include "../includes/board_state.h"
#include "../includes/file_i.h"
#include "../includes/fuzzy_clustering.h"
#include "../includes/post_processing.h"
#include <iomanip>
#include <iostream>

const std::string file_path = "../data/tic-tac-toe.data.txt";

int main(int argc, char** argv) {
    std::vector<BoardState> states;
    read_data_file(file_path, states);
    FuzzyClustering fc(2, 2.0, 150, 10e-10, 2, states);

    // Run 100 times
    std::vector<std::vector<double>> fuzzy;
    std::vector<std::set<BoardState>> best_G;
    get_best_result(fc, 100, fuzzy);

    // Get hard partition from best fuzzy partition
    std::vector<std::vector<bool>> hard;
    get_hard_partition(fuzzy, hard);

    std::vector<BoardState> E = fc.get_E();

    std::cout << std::fixed;
    std::cout << std::setprecision(3);

    // Print Partitions
    print_fuzzy_partition(fuzzy, E);
    std::cout << std::endl;
    print_hard_partition(hard, E);

    // Print Medoids
    print_medoids();

    // Calculate Adjusted Rand Index
    std::cout << "------------------" << std::endl;
    double ari = adjusted_rand_index(hard, E);
    std::cout << "Adjusted Rand Index: " << ari << std::endl;

    return 0;
}