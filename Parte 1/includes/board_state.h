#ifndef _BOARD_STATE_H
#define _BOARD_STATE_H

#include <fstream>
#include <functional>
#include <string>
#include <vector>

class BoardState {
public:
    std::string board;
    bool x_wins;
    int id;

    BoardState() : x_wins(false) {}

    BoardState(std::string board_, bool x_wins_, int id_) : board(board_), x_wins(x_wins_), id(id_) { }

    int distance(const BoardState& rhs) const {
        int size = this->board.size();
        int acc = 0;
        for (int i = 0; i < size; ++i) 
            acc += delta(this->board[i], rhs.board[i]);
        return acc;
    }

    friend std::ostream& operator<<(std::ostream& os, const BoardState& bs) {
        os << bs.board << "(" << bs.x_wins << ")";
        return os;
    }

    friend bool operator==(const BoardState& lhs, const BoardState& rhs) {
        return lhs.board == rhs.board;
    }

    friend bool operator<(const BoardState& lhs, const BoardState& rhs) {
        return lhs.board < rhs.board;
    }

    ~BoardState() { }

protected:
    inline int delta(const char& s1, const char& s2) const {
        return s1 != s2;
    }

};

#endif //_BOARD_STATE_H