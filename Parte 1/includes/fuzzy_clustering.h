#ifndef _FUZZY_CLUSTERING_H
#define _FUZZY_CLUSTERING_H

#include "board_state.h"
#include <cmath>
#include <functional>
#include <random>
#include <set>
#include <vector>

class FuzzyClustering {
public:
    struct Rand_Int {
        Rand_Int() {}

        Rand_Int(unsigned seed, int low, int high) : re(seed), dist { low, high } {}

        int operator()() { return dist(re); }

        std::default_random_engine re;
        std::uniform_int_distribution<int> dist;
    };

    FuzzyClustering(int K, double m, int T, double eps, int q, std::vector<BoardState>& E);

    // Calculate Dissimilarity matrix from objects
    void calculate_dissimilarity();
    
    // Randomly select K distinct prototype Gk(0) from {A c E : |A|  = q}
    void random_prototypes();

    // Initialization step
    void initialize();

    // Step 1 procedure for best prototypes computation
    void compute_prototypes();

    // Step 2 function for best fuzzy partition computation
    void compute_partition();

    // Adequancy criterion computation
    double compute_adequancy();

    // Run full algorithm for T iterations
    void run();

    /* ------- */
    /* Getters */
    /* ------- */
    
    int get_K();

    int get_T();

    int get_q();

    int get_n();

    double get_m();

    double get_eps();

    double get_J();

    std::vector<BoardState> get_E();

    std::vector<std::set<BoardState>> get_G();

    std::vector<std::vector<double>> get_U();

    std::vector<std::vector<int>> get_D();

    /* ------- */
    /* Setters */
    /* ------- */

    void set_K(int K);

    void set_m(double m);

    void set_T(int T);

    void set_q(int q);

    void set_n(int n);

    void set_eps(double eps);

    void set_J(double J);

    void set_E(std::vector<BoardState>& E);

    void set_G(std::vector<std::set<BoardState>>& G);

    void set_U(std::vector<std::vector<double>>& U);

    void set_D(std::vector<std::vector<int>>& D);

protected:
    template<typename T>
    bool compare(T& a, T& b) {
        return (fabs(a - b) < this->eps);
    }

private:
    int K; // Number of clusters
    int T; // Number of iterations
    int q; // Cluster cardinality
    int n; // Number of objects

    double m; // Fuziness parameter
    double eps; // Stopping criterion
    double J; // Adequancy criterion

    Rand_Int rng;

    std::vector<BoardState> E; // Objects
    std::vector<std::set<BoardState>> G; // Set of prototypes
    std::vector<std::vector<double>> U; // Vector of membership degrees
    std::vector<std::vector<int>> D; // Dissimilarity matrix

};

#endif // _FUZZY_CLUSTERING_H