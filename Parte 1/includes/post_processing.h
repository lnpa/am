#ifndef _POST_PROCESSING_H
#define _POST_PROCESSING_H

#include "board_state.h"
#include "fuzzy_clustering.h"

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>

double eps = std::numeric_limits<double>::epsilon();
std::vector<std::set<BoardState>> best_G;

// Runs the algorithm 100 times and chooses the best partition according to the adequancy
void get_best_result(FuzzyClustering& fc, int num_executions, std::vector<std::vector<double>>& partition) {
    double J_min = std::numeric_limits<double>::max();
    for (int i = 0; i < num_executions; ++i) {
        fc.run();
        if (fc.get_J() < J_min) {
            J_min = fc.get_J();
            partition = fc.get_U();
            best_G = fc.get_G();
        }
    }
}

// Returns the corresponding hard partition to a fuzzy partition
void get_hard_partition(std::vector<std::vector<double>>& fuzzy, std::vector<std::vector<bool>>& hard) {
    //srand(time(NULL));
    for (auto& fuzzy_row : fuzzy) {
        std::vector<bool> hard_row;
        double max = *(std::max_element(fuzzy_row.begin(), fuzzy_row.end()));
        for (double& u_ik : fuzzy_row) {
            if (fabs(max - u_ik) < eps) {
                hard_row.push_back(true);
            }
            else {
                hard_row.push_back(false);
            }
        }
        int true_elms = 0;
        bool change = false;
        for (unsigned int i = 0; i < hard_row.size(); ++i) {
            if (hard_row[i] && true_elms == 1) {
                change = true;
                break;
            }
            else if (hard_row[i]) {
                true_elms++;
            }
        }
        if (change) {
            unsigned int chosen = rand() % hard_row.size();
            for (unsigned int i = 0; i < hard_row.size(); ++i) {
                if (hard_row[i] && i != chosen) {
                    hard_row[i] = false;
                }
            }
        }
        hard.push_back(hard_row);
    }
}

// From a given fuzzy partition and a list of objects, print the U matrix
void print_fuzzy_partition(std::vector<std::vector<double>>& fuzzy, std::vector<BoardState>& E) {
    std::cout << "|---------------|" << std::endl;
    std::cout << "|Fuzzy Partition|" << std::endl;
    std::cout << "|---------------|" << std::endl;
    std::cout << std::endl;

    for (unsigned int i = 0; i < E.size(); ++i) {
        for (unsigned int k = 0; k < fuzzy[0].size(); ++k) {
            std::cout << "u_" << i + 1 << "_" << k + 1 << ": " << fuzzy[i][k] << " ";
        }
        std::cout << std::endl;
    }
}

// From a given hard partition and a list of objects, print the objects in the correct place
void print_hard_partition(std::vector<std::vector<bool>>& hard, std::vector<BoardState>& E, bool calculate_distances = false) {
    std::cout << "|--------------|" << std::endl;
    std::cout << "|Hard Partition|" << std::endl;
    std::cout << "|--------------|" << std::endl;
    std::cout << std::endl;

    for (unsigned int k = 0; k < hard[0].size(); ++k) {
        std::cout << "Partition " << k + 1 << std::endl;
        int count_x_wins = 0;
        int count_not_x_wins = 0;
        
        double avg_dist = 0.0;
        double op_avg_dist = 0.0;
        double n_samples = 0.0;

        int line_break = 6;
        int printed = 0;

        for (unsigned int i = 0; i < E.size(); ++i) {
            if (hard[i][k]) {

                // Calculate average distance for testing purposes
                if (calculate_distances) {
                    n_samples += 2.0;
                    for (auto& g_ki : best_G[k]) {
                        avg_dist += E[i].distance(g_ki);
                    }

                    for (auto& g_ki : best_G[(k + 1) % 2]) {
                        op_avg_dist += E[i].distance(g_ki);
                    }
                }

                std::cout << E[i] << " ";

                if (++printed == line_break) {
                    printed %= line_break;
                    std::cout << std::endl;
                }
                
                if (E[i].x_wins) {
                    count_x_wins++;
                }
                else {
                    count_not_x_wins++;
                }
            }
        }

        std::cout << std::endl;
        std::cout << "Positive (X wins): " << count_x_wins << std::endl;
        std::cout << "Negative (X doesn't win):" << count_not_x_wins << std::endl;

        if (calculate_distances) {
            std::cout << "Average distance: " << avg_dist / n_samples << std::endl;
            std::cout << "Average distance to opposite Cluster: " << op_avg_dist / n_samples << std::endl;
        }
        
        std::cout << std::endl;
    }
}

// Prints the Prototypes
void print_medoids() {
    std::cout << "|--------|" << std::endl;
    std::cout << "|Clusters|" << std::endl;
    std::cout << "|--------|" << std::endl;
    std::cout << std::endl;

    auto G = best_G;
    int k = 1;
    for (auto& g_k : G) {
        std::cout << "Cluster " << k << ":" << std::endl;
        int i = 1;
        for (auto& e : g_k) {
            std::cout << "G_" << k << i << ": " << e << std::endl;
            ++i;
        }
        std::cout << std::endl;
        k++;
    }
}

// Auxiliar function to rand index
double n_choose_2(int n) {
    return (n * (n - 1)) / 2.0;
}

// Calculate Adjusted Rand Index of the hard partition and the correct partition
double adjusted_rand_index(std::vector<std::vector<bool>>& hard, std::vector<BoardState>& E) {
    double ari = 0.0;
    std::vector<std::vector<int>> n_ij;
    std::vector<int> sums_a;
    std::vector<int> sums_b (2);
    for (unsigned int k = 0; k < hard[0].size(); ++k) {
        std::vector<int> row;
        int count_true = 0;
        int count_false = 0;
        for (unsigned int i = 0; i < hard.size(); ++i) {
            if (hard[i][k] && E[i].x_wins) {
                count_true++;
            }
            else if (hard[i][k]) {
                count_false++;
            }
        }
        row.push_back(count_true);
        row.push_back(count_false);
        n_ij.push_back(row);
    }
    for (unsigned int i = 0; i < n_ij.size(); ++i) {
        int sum_row = 0;
        for (unsigned int j = 0; j < n_ij[0].size(); ++j) {
            sum_row += n_ij[i][j];
            sums_b[j] += n_ij[i][j];
        }
        sums_a.push_back(sum_row);
    }
    double sum_n_ij_2 = 0.0;
    double sum_a_i_2 = 0.0;
    double sum_b_j_2 = 0.0;
    double sum_n_2 = n_choose_2(E.size());
    for (unsigned int i = 0; i < n_ij.size(); ++i) {
        sum_a_i_2 += n_choose_2(sums_a[i]);
        for (unsigned int j = 0; j < n_ij[0].size(); ++j) {
            sum_n_ij_2 += n_choose_2(n_ij[i][j]);
        }
    }
    for (unsigned int j = 0; j < n_ij[0].size(); ++j) {
            sum_b_j_2 += n_choose_2(sums_b[j]);
    }
    double numerator = sum_n_ij_2 - ((sum_a_i_2 * sum_b_j_2) / sum_n_2);
    double denominator = ((sum_a_i_2 + sum_b_j_2) / 2.0) - ((sum_a_i_2 * sum_b_j_2) / sum_n_2);
    ari = numerator / denominator;
    return ari;
}

#endif // _POST_PROCESSING_H