#ifndef _FILE_I_H
#define _FILE_I_H

#include "board_state.h"
#include <iostream>
#include <fstream>
#include <sstream>

BoardState read_line(const std::string& s, int id) {
    BoardState board_state;
    std::stringstream ss(s);
    std::string item;
    for (int i = 0; i < 9; ++i) {
        std::getline(ss, item, ',');
        board_state.board.push_back(item[0]);
    }
    std::getline(ss, item, ',');
    board_state.x_wins = item == "positive" ? true : false;
    board_state.id = id;
    return board_state;
}

void read_data_file(std::string filename, std::vector<BoardState>& states) {
    std::ifstream ifs(filename, std::ifstream::in);
    std::string line;
    int id = 0;
    while (ifs.good()) {
        ifs >> line;
        states.push_back(read_line(line, id++));
    }
    ifs.close();
}

#endif //_FILE_I_H