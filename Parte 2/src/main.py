from file_input import read_data_file
from knn_classifier import KNNClassifier
from bayes_classifier import BayesClassifier
from svm_classifier import SVMClassifier
from combine_classifiers import CombineClassifiers
from mlp_classifier import MLPClassifier
from k_fold import run_classifiers
import scipy.sparse.linalg.isolve.iterative
import statistics
import tests

observation = "\nFor all calculations ahead, each run of 10-fold cross validation was accounted for as a single dataset"

def main():
    filename = "../data/tic-tac-toe.data.txt"
    states = []
    read_data_file(filename, states)
    tests_sets = []
    num_repetitions = 10
    alpha = 0.05
    classifiers = {'knn': {'instance': KNNClassifier(), 'results': []}, 'svm': {'instance': SVMClassifier(), 'results': []}, 'bayes': {'instance': BayesClassifier(), 'results': []}, 'combined': {'instance': CombineClassifiers(), 'results': []}, 
    'mlp': {'instance': MLPClassifier(), 'results': []}}

    for i in range(num_repetitions):
        print "10-fold Cross Validation", (i + 1)
        run_classifiers(states, 10, classifiers, tests_sets)

    results, matrix = statistics.estimation(classifiers, tests_sets, num_repetitions)
    print observation
    print "\n\n\nError estimates\n"

    for classifier in results:
        print classifier
        print "Average Error:", results[classifier]['f']
        print "Standard Deviation:", results[classifier]['sd']
        print "Confidence Interval: [", results[classifier]['conf_int'][0], ", ", results[classifier]['conf_int'][1], "]"
        print "\n"

    print "\n\n\nClassifier Tests\n"
    print "Friedman Test\n"
    test_results = tests.friedman_test(matrix, alpha)

    if isinstance(test_results, tuple):
        print "\nThe Null Hyphothesis was proven for alpha =", alpha
    else:
        print "\nThe Null Hyphotesis was proven false for alpha =", alpha
        print "\nNemenyi Test\n"
        for pair in test_results:
            print pair, ":"
            print "Difference, Relevant: ", test_results[pair]


if __name__ == "__main__":
    main()