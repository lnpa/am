class BoardState:
    def __init__(self, _board = [], _x_wins = False):
        self.x_wins = _x_wins
        self.board = map(lambda s: (1 if s=='x' else (0 if s=='o' else -1)), _board)

    def distance(self, rhs):
        size = len(self.board)
        acc = 0
        for i in range(size):
            acc += delta(self.board[i], rhs.board[i])
        return acc

    def delta(self, s1, s2):
        return s1 != s2

    def __str__(self):
        return str(self.board) + " (" + str(self.x_wins) + ")"