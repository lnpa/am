import math

class BayesClassifier:

    def __init__(self):
        self.p = []
        self.q = []
        self.r = []
        self.posterioris = []
        self.priori = [0.0, 0.0]
        self.num_boards = 0.0
        self.init_parameters(self.p)
        self.init_parameters(self.q)
        self.init_parameters(self.r)

    def init_parameters(self, param):
        line = [0.0]*2
        for i in range(9):
            param.append(line[:])

    def train(self, states):
        for state in states:
            self.num_boards += 1
            index = 0 if state.x_wins else 1
            self.priori[index] += 1
            for i in range(9):
                self.p[i][index] += (state.board[i] * (state.board[i] + 1)) / 2.0
                self.q[i][index] += (1 - (state.board[i] ** 2))
                self.r[i][index] += (state.board[i] * (state.board[i] - 1)) / 2.0
        for i in range(9):
            for j in range(2):
                self.p[i][j] /= self.priori[j]
                self.q[i][j] /= self.priori[j]
                self.r[i][j] /= self.priori[j]
        for j in range(2):
            self.priori[j] /= self.num_boards

    def classify_board(self, board_state):
        log_probs = [math.log(self.priori[0]), math.log(self.priori[1])]
        for j in range(2):
            for i in range(9):
                x_i = board_state.board[i]
                term_prod = self.p[i][j] ** ((x_i * (x_i + 1)) / 2.0)
                term_prod *= self.q[i][j] ** (1 - (x_i ** 2))
                term_prod *= self.r[i][j] ** ((x_i * (x_i - 1)) / 2.0)
                log_probs[j] += math.log(term_prod)
        denom = sum([math.exp(log_prob) for log_prob in log_probs])
        post_true = math.exp(log_probs[0] - math.log(denom))
        post_false = math.exp(log_probs[1] - math.log(denom))
        self.posterioris.append([post_true, post_false])
        if log_probs[0] >= log_probs[1]:
            # True = x_wins
            return True
        else:
            return False

    def classify(self, states, classes):
        for state in states:
            classes.append(self.classify_board(state))
