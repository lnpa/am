from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.grid_search import GridSearchCV
from sklearn.svm import SVC
import math
import numpy as np

class SVMClassifier:

    def __init__(self):
        self.clf = SVC(kernel = 'rbf', class_weight = 'balanced')
              
    def train(self, states):
        boards = [state.board for state in states]
        labels = [state.x_wins for state in states]

        C_range = np.logspace(-5, 15, 11, True, 2.0)
        gamma_range = np.logspace(-15, 3, 10, True, 2.0)
        param_grid = dict(gamma = gamma_range, C = C_range)
        cv = StratifiedShuffleSplit(labels, n_iter = 5, test_size = 0.2, random_state = 42)
        self.SVM = GridSearchCV(self.clf, param_grid = param_grid, cv = cv)

        self.SVM.fit(boards, labels)

    def classify(self, states, classes):
        boards = [state.board for state in states]
        classes.extend(self.SVM.predict(boards).tolist())
        
