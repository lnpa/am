from board_state import BoardState

def read_line(line, states):
    board = []
    items = line.split(',')
    for i in range(9):
        board.append(items[i])
    x_wins = True if items[9] == "positive" else False
    states.append(BoardState(board, x_wins))

def read_data_file(filename, states):
    with open(filename, "rb") as f:
        for line in f:
            line = line.rstrip()
            read_line(line, states)

def calculate_prioris(states):
    true_priori = 0;
    false_priori = 0;
    for state in states:
        if state.x_wins:
            true_priori += 1;
        else: 
            false_priori += 1;
    total = true_priori + false_priori
    return float(true_priori) / total, float(false_priori) / total