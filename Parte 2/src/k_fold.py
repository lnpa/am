from sklearn.cross_validation import StratifiedKFold
from datetime import datetime

def k_fold_division(states, k):
    labels = [state.x_wins for state in states]
    return StratifiedKFold(labels, k, shuffle = True)

def run_classifier(classifier, train_set, test_set, results):
    classifier.train(train_set)
    classes = []
    classifier.classify(test_set, classes)
    results.append(classes)

def run_classifiers(states, k, classifiers, tests):
    i = 1
    folds = k_fold_division(states, k)
    for train, test in folds:
        train_set = [st for st in states if states.index(st) in train]
        test_set = [st for st in states if states.index(st) in test]
        tests.append(test_set)
        i += 1
        for classifier_name in classifiers:
            classifier = classifiers[classifier_name]['instance']
            results = classifiers[classifier_name]['results']
            classifier.__init__()
            run_classifier(classifier, train_set, test_set, results)