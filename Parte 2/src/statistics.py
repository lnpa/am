from scipy import stats
import math
from file_input import read_data_file
from knn_classifier import KNNClassifier
from bayes_classifier import BayesClassifier
from svm_classifier import SVMClassifier
from combine_classifiers import CombineClassifiers
from k_fold import run_classifiers

alpha = 0.05

def estimation(classifiers, tests, num_runs):
    results = {}
    for classifier in classifiers:
        results[classifier] = {}
        results[classifier]['avg_f'] = []
        results[classifier]['avg_sd'] = []
        res = classifiers[classifier]['results']
        lst_f = []
        lst_sd = []

        for result, test in zip(res, tests):
            num_err = 0
            n = len(test)

            for label, board in zip(result, test):
                if board.x_wins != label:
                    num_err += 1
            
            f = num_err / float(n)
            lst_f.append(f)
            sd = math.sqrt((f * (1 - f)) / float(n))
            lst_sd.append(sd)

        for i in range(num_runs):
            f_run = lst_f[10 * i : 10 * (i + 1)]
            sd_run = lst_sd[10 * i : 10 * (i + 1)]
            results[classifier]['avg_f'].append(sum(f_run) / float(len(f_run)))
            results[classifier]['avg_sd'].append(sum(sd_run) / float(len(sd_run)))

        avg_f = results[classifier]['avg_f']
        avg_sd = results[classifier]['avg_sd']

        results[classifier]['f'] = sum(avg_f) / float(num_runs)
        results[classifier]['sd'] = sum(avg_sd) / float(num_runs)

        t = stats.t.ppf(1 - (alpha / 2), num_runs - 1)
        f = results[classifier]['f']
        second_term = t * (math.sqrt((f * (1 - f)) / float(num_runs)))
        conf_int = (f - second_term, f + second_term)

        conf_int = (conf_int[0] if conf_int[0] > 0.0 else 0.0, conf_int[1])
        results[classifier]['conf_int'] = conf_int

    matrix = generate_matrix(results, num_runs)
    return results, matrix

def generate_matrix(classifiers, num_runs):
    matrix = []
    for i in range(num_runs):
        row = []
        for classifier in sorted(classifiers):
            row.append(classifiers[classifier]['avg_f'][i])
        matrix.append(row)
    return matrix




