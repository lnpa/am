from scipy.stats import friedmanchisquare
from scipy.stats import rankdata
from scipy.stats import f
from operator import itemgetter
from math import sqrt

nemenyi_critval = [
    #a 0.01 0.05 0.10      N
    [2.576, 1.960, 1.645], # 2
    [2.913, 2.344, 2.052], # 3
    [3.113, 2.569, 2.291], # 4
    [3.255, 2.728, 2.460], # 5
    [3.364, 2.850, 2.589], # 6
    [3.452, 2.948, 2.693], # 7
    [3.526, 3.031, 2.780], # 8
    [3.590, 3.102, 2.855], # 9
    [3.646, 3.164, 2.920], # 10
    [3.696, 3.219, 2.978], # 11
    [3.741, 3.268, 3.030], # 12
    [3.781, 3.313, 3.077], # 13
    [3.818, 3.354, 3.120], # 14
    [3.853, 3.391, 3.159], # 15
    [3.884, 3.426, 3.196], # 16
    [3.914, 3.458, 3.230], # 17
    [3.941, 3.489, 3.261], # 18
    [3.967, 3.517, 3.291], # 19
    [3.992, 3.544, 3.319], # 20
    [4.015, 3.569, 3.346], # 21
    [4.037, 3.593, 3.371], # 22
    [4.057, 3.616, 3.394], # 23
    [4.077, 3.637, 3.417], # 24
    [4.096, 3.658, 3.439], # 25
    [4.114, 3.678, 3.459], # 26
    [4.132, 3.696, 3.479], # 27
    [4.148, 3.714, 3.498], # 28
    [4.164, 3.732, 3.516], # 29
    [4.179, 3.749, 3.533], # 30
    [4.194, 3.765, 3.550], # 31
    [4.208, 3.780, 3.567], # 32
    [4.222, 3.795, 3.582], # 33
    [4.236, 3.810, 3.597], # 34
    [4.249, 3.824, 3.612], # 35
    [4.261, 3.837, 3.626], # 36
    [4.273, 3.850, 3.640], # 37
    [4.285, 3.863, 3.653], # 38
    [4.296, 3.876, 3.666], # 39
    [4.307, 3.888, 3.679], # 40
    [4.318, 3.899, 3.691], # 41
    [4.329, 3.911, 3.703], # 42
    [4.339, 3.922, 3.714], # 43
    [4.349, 3.933, 3.726], # 44
    [4.359, 3.943, 3.737], # 45
    [4.368, 3.954, 3.747], # 46
    [4.378, 3.964, 3.758], # 47
    [4.387, 3.973, 3.768], # 48
    [4.395, 3.983, 3.778], # 49
    [4.404, 3.992, 3.788], # 50
]

def nemenyi_critical_difference(alpha, k, n):
    col = -1
    
    if alpha == 0.01:
        col = 0
    elif alpha == 0.05:
        col = 1
    elif alpha == 0.10:
        col = 2
    else:
        print 'ERROR. alpha must be 0.01, 0.05 or 0.10.'
        return None

    if k < 2 or k > 50:
        print 'ERROR. k must be between 2 and 50.'
        return None

    critval = nemenyi_critval[k - 2][col]

    critdiff = critval * sqrt(float(k * (k + 1)) / (6 * n))

    return critdiff

def nemenyi_test(avg_ranks, n, k, alpha):
    CD = nemenyi_critical_difference(alpha, k, n)
    differences = {}
    # 0 = bayes, 1 = combined, 2 = knn, 3 = mlp, 4 = svm
    classifiers = { 0: 'Bayes', 1: 'Combined', 2: 'Knn', 3: 'MLP', 4: 'SVM'}
    for i in range(k):
        for j in range(i + 1, k):
            diff = avg_ranks[i] - avg_ranks[j]
            relevant = False if abs(diff) < CD else True
            differences[(classifiers[i], classifiers[j])] = (diff, relevant)

    return differences

# Rank classifiers on a single data set
def rank_classifiers(errors, ranks, dataset):
    errors_ranks = enumerate(errors[dataset])
    # Sort by error values
    errors_sorted = sorted(errors_ranks, key = itemgetter(1))

    ranked = rankdata([b for (a, b) in errors_sorted])
    indexes = [a for (a, b) in errors_sorted]
    errors_sorted = zip(indexes, ranked)

    for (a, b) in errors_sorted:
        ranks[dataset][a] = b

def get_avg_ranks(n, k, ranks):
    avg_ranks = [0.0] * k
    for j in range(k):  
        for i in range(n):
            avg_ranks[j] += ranks[i][j]
        avg_ranks[j] /= n    
    return avg_ranks

def chi_square(n, k, avg_ranks):
    outer = (float(12 * n)) / (k * (k + 1))
    inner = sum(map(lambda x: x ** 2.0, avg_ranks))
    return outer * (inner - float(k * ((k + 1) ** 2)) / 4.0) 

def F_distribution(n, k, chi):
    denom = (n * (k - 1) - chi)
    if denom == 0:
        return float("inf")
    return float((n - 1) * chi) / denom

def friedman_test(errors, alpha):
    n = len(errors)
    k = len(errors[0])

    ranks = []
    line = [0.0] * k
    for i in range(n):
        ranks.append(line[:])

    # Rank classifiers for each data set
    for i in range(len(errors)):
        rank_classifiers(errors, ranks, i)

    avg_ranks = get_avg_ranks(n, k, ranks)
    print "Average ranks of the classifiers"
    classifiers = { 0: 'Bayes', 1: 'Combined', 2: 'Knn', 3: 'MLP', 4: 'SVM'}
    
    for i in range(len(avg_ranks)):
        print classifiers[i], ": ", avg_ranks[i]

    chi = chi_square(n, k, avg_ranks)

    Ff = F_distribution(n, k, chi)
    print "Ff value: ", Ff

    # Test against null hypothesis
    Ff1 = f.ppf(1.0 - alpha, (k - 1), (k - 1) * (n - 1))
    print "Critical value: ", Ff1

    # Reject the null-hypothesis
    if Ff > Ff1:
        # Perform nemenyi test
        differences = nemenyi_test(avg_ranks, n, k, alpha)
        return differences
    else:
        return avg_ranks, chi, Ff, Ff1
