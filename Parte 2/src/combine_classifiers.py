from bayes_classifier import BayesClassifier
from knn_classifier import KNNClassifier

class CombineClassifiers:

    def __init__(self, true_priori = 0.653444676409, \
                 false_priori = 0.346555323591):
        self.true_priori = true_priori
        self.false_priori = false_priori
        self.knn = KNNClassifier()
        self.bayes = BayesClassifier()

    def train(self, states):
        self.knn.train(states)
        self.bayes.train(states)

    def classify(self, states, classes):
        self.classes_1 = []
        self.classes_2 = []
        self.knn.classify(states, self.classes_1)
        self.bayes.classify(states, self.classes_2)
        self.combine(states, classes)

    def combine_state(self, state, i):
        true_sum = (-1 * self.true_priori) + self.knn.posterioris[i][1] + \
                   self.bayes.posterioris[i][0]

        false_sum = (-1 * self.false_priori) + self.knn.posterioris[i][0] + \
                    self.bayes.posterioris[i][1]

        return true_sum > false_sum

    def combine(self, states, classes):
        for i, state in enumerate(states):
            classes.append(self.combine_state(state, i))