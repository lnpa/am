from sknn.backend import lasagne
from sknn.mlp import Classifier, Layer
from numpy import array

class MLPClassifier:
    def __init__(self, n_layers = 1, nodes_per_layer = 35):
        layers_ = []
        for i in range(n_layers):
            layers_.append(Layer("Sigmoid", units=nodes_per_layer))
        self.MLP = Classifier(
            layers= layers_ + [Layer("Softmax")],
            learning_rate=0.03,
            learning_rule="rmsprop")

    def train(self, board_states):
        X = array(map(lambda bs: list(bs.board), board_states))
        Y = array(map(lambda bs: bs.x_wins, board_states))
        self.MLP.fit(X, Y)

    def classify(self, board_states, classes):
        X = array(map(lambda bs: list(bs.board), board_states))
        classes.extend(self.MLP.predict(X))
