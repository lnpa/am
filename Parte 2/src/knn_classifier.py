from sklearn.neighbors import KNeighborsClassifier

class KNNClassifier:

    def __init__(self, n_neighbors = 17, algorithm = "auto"):
        self.n_neighbors = n_neighbors
        self.algorithm = algorithm
        self.construct_classifier()

    def distance(self, board1, board2):
        dist = 0
        for (elm1, elm2) in zip(board1, board2):
            dist += 1 if (elm1 != elm2) else 0
        return dist

    def construct_classifier(self):
        self.knn = KNeighborsClassifier(self.n_neighbors, algorithm = self.algorithm, metric = self.distance)

    def train(self, states):
        boards = [state.board for state in states]
        labels = [state.x_wins for state in states]
        self.knn.fit(boards, labels)

    def classify(self, states, classes):
        boards = [state.board for state in states]
        classes.extend(self.knn.predict(boards).tolist())
        self.posterioris = self.knn.predict_proba(boards)
